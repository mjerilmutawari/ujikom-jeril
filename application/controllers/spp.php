<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class spp extends CI_Controller 
{

	public function __construct()
   {

      parent::__construct();
      $this->load->model('spp_model');
	  }

   //read all
	public function index()
   {
     $data['query'] = $this->spp_model->read();
	   $this->load->view('spp/index', $data);
   }

   public function edit($id_spp)
   {
      $data['query'] = $this->spp_model->read_by_id($id_spp);
      $this->load->view('spp/edit', $data);
   }   
   
    public function add()
   {
      $this->load->view('spp/add');
   }

    public function insert()
   {
      $tahun = $this->input->post('tahun');
      $nominal = $this->input->post('nominal');

      echo $tahun . " - " . $nominal;

      $data['query'] = $this->spp_model->create();

      redirect('spp');
   }

    public function delete($id_spp)
	{
		$this->spp_model->delete($id_spp);
		 redirect('spp');
	}
}	

        
   
		
	    


	



